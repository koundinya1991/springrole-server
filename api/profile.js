import request  from 'request-promise';

const user = `c4f4c607-e9c8-4acb-9893-93c541bddf35`;
const baseUrl = `https://beta.springrole.com/api/v1/user/`;



function constructEndPoint(endPoint) {
    let res;
    switch (endPoint) {
      case 'work':
        res =`${baseUrl}${user}/work-experience`;
        break;
      case 'skills':
        res = `${baseUrl}${user}/profile/skills`;
        break;
      case 'edu':
        res = `${baseUrl}${user}/education`;
        break;
      default:
        res ='no end point';
    }
    return res;
}
function get(endPoint) {
  let res = request(constructEndPoint(endPoint)).then(data => data).catch(error => error);
  return res;
}

export default get;
