import 'babel-polyfill';
import express from 'express';
const app = express();
import routes from './routes';


let port = process.env.PORT || 8888;

app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
  res.setHeader('Access-Control-Allow-Headers', 'Origin,X-Requested-With, Content-Type');
  next();
});
app.use('/api', routes);

app.listen(port, ()=> {
  console.log(`listening on port ${port}`);
});
