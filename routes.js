import express from 'express';

let routes = express.Router();
import get from './api/profile';

routes.get('/skills', async(req, res)=>{
  let resp = await get('skills');
  res.send({
    skills: resp
  })
});
routes.get('/work', async (req, res)=>{
  let resp = await get('work');
  res.send({
    work: resp
  })
});
routes.get('/edu', async (req, res)=>{
  let resp = await get('edu');
  res.send({
    edu: resp
  })
});

export default routes;
